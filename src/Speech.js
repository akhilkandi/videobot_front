'use strict'
import React, { Component } from 'react'
import YouTube from 'react-youtube';
import axios from "axios";


//const SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
window.SpeechRecognition = window.webkitSpeechRecognition || window.SpeechRecognition;
//const recognition = new SpeechRecognition()
const recognition = new window.SpeechRecognition();


recognition.continous = true
recognition.interimResults = true
recognition.lang = 'en-US'


class Speech extends Component {

  constructor() {
    super()
    this.state = {
      listening: false,
      link:'yZIummTz9mM'
    }
    this.toggleListen = this.toggleListen.bind(this)
    this.handleListen = this.handleListen.bind(this)
  }

  toggleListen() {
    this.setState({
      listening: !this.state.listening
    }, this.handleListen)
  }

  _onReady(event) {
    event.target.pauseVideo();
  }

  handleListen(){
    console.log('listening?', this.state.listening)

    if (this.state.listening) {
      recognition.start()
      recognition.onend = () => {
        console.log("...continue listening...")
        recognition.start()
      }

    } else {
      recognition.stop()
      recognition.onend = () => {
        console.log("Stopped listening per click")
      }
    }

    recognition.onstart = () => {
      console.log("Listening!")
    }

    let finalTranscript = ''
    recognition.onresult = event => {
      let interimTranscript = ''
      console.log("here")
      for (let i = event.resultIndex; i < event.results.length; i++) {
        const transcript = event.results[i][0].transcript;
        console.log(transcript);
        if (event.results[i].isFinal) finalTranscript += transcript + ' ';
        else interimTranscript += transcript;
      }
      document.getElementById('interim').innerHTML = interimTranscript
      console.log(finalTranscript);
      document.getElementById('final').innerHTML = finalTranscript
      //send it to node through axios

      //get the link and change the state and so it rerenders.
      axios.get(‘/videobot/?text=finalTranscript’,{data:’data’}).then((res)=>{
        console.log(response.data);
        this.setState({link:{response.data.link}});
      });

  }
}

  render() {
    const opts = {
      height: '390',
      width: '640',
      playerVars: { // https://developers.google.com/youtube/player_parameters
        autoplay: 1
      }
    };
    console.log(this.state.link);
    return (
      <div style={container}>
        <button id='microphone-btn' style={button} onClick={this.toggleListen} />
        <div id='interim' style={interim}></div>
        <div id='final' style={final}></div>
        <YouTube
        videoId={this.state.link}
        opts={opts}
        onReady={this._onReady}
      />
      </div>
    );
  }
}

export default Speech



const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center'
  },
  button: {
    width: '60px',
    height: '60px',
    background: 'lightblue',
    borderRadius: '50%',
    margin: '6em 0 2em 0'
  },
  interim: {
    color: 'gray',
    border: '#ccc 1px solid',
    padding: '1em',
    margin: '1em',
    width: '300px'
  },
  final: {
    color: 'black',
    border: '#ccc 1px solid',
    padding: '1em',
    margin: '1em',
    width: '300px'
  }
}

const { container, button, interim, final } = styles
